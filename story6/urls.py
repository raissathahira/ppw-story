from django.urls import path
from . import views
from django.contrib import admin

app_name = 'story6'

urlpatterns = [
    path('kegiatan/', views.acts, name= 'kegiatan'),
    path('kegiatan/data', views.acts, name= 'kegiatan'),
    path('daftar/', views.datact, name="daftar" ),
    path('daftar/data', views.datact, name="daftar" ),
]

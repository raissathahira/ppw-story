# Generated by Django 3.1.1 on 2020-10-24 00:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0003_auto_20201023_1727'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='regist',
            options={'ordering': ['nama']},
        ),
        migrations.AlterModelTable(
            name='regist',
            table=None,
        ),
    ]

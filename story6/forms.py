from django.core import validators
from django import forms
from .models import act, regist


class actform(forms.ModelForm):
    required_css_class = "required"
    class Meta:
        model = act
        fields = ['nama']
        widgets ={
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
        }

class registerform(forms.ModelForm):
    required_css_class = "required"
    class Meta:
        model = regist
        fields = ['nama', 'act']
        widgets ={
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
            'desc' : forms.TextInput(attrs={'class': 'dropdown-menu'}),
        }
        
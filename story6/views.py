from django.shortcuts import render, redirect
from .models import act, regist
from .forms import actform, registerform
from django.http import HttpResponseRedirect

# Create your views here.
def acts(request):
    form = actform(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/kegiatan')
    else:
        stud = act.objects.all()
    return render(request, 'kegiatan.html', {'form': form, 'stud': stud,})

def datact(request):
    form= registerform(request.POST or None)
    dataact = act.objects.all()
    datareg = regist.objects.all()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/daftar')
        else:
            data = {
                'form' : form,
                'act' : act,
                'cek' : 'failed',
                'dataact': dataact,
                'datareg' : datareg,
	            }	

            return render(request, 'listact.html', data)
    else:
        data = {
                'form' : form,
                'act' : act,
                'cek' : 'failed',
                'dataact':dataact,
                'datareg' : datareg,
	            }
        return render(request, 'listact.html', data)




# Create your tests here.
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .models import act, regist
from .forms import actform,  registerform
from .views import acts, datact
# Create your tests here.
class TestKegiatan(TestCase):
    def testurl_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)
    
    def url_is_exist(self):
        response = Client().get('/kegiatan/data')
        self.assertEqual(response.status_code, 200)
    
    def test1_using_template(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan.html')
    
    def test2_using_template(self):
        response = Client().get('/kegiatan/data')
        self.assertTemplateUsed(response, 'kegiatan.html')
    
    def test3_using_template(self):
        response = Client().get('/daftar/data')
        self.assertTemplateUsed(response, 'listact.html')

    def test_url_exist(self):
        response = Client().get('/daftar/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_exist(self):
        response = Client().get('/daftar/data')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/daftar/')
        self.assertTemplateUsed(response, 'listact.html')
    

    def test_url_post_is_exist(self):
        response = Client().post('/daftar', data={'nama':'kucing'})
        self.assertEqual(response.status_code, 301)


    def form_validation_accepted(self):
        form = registerform(data={'nama': 'yow'})
        self.assertTrue(form.is_valid())
    
    def test_activity_add_using_func(self):
        found = resolve('/daftar/')
        self.assertEqual(found.func, datact)

    



@tag('functional')
class IndexFunctionalTest(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()




from django.forms import ModelForm, Textarea
from django.db import models

# Create your models here.
class act(models.Model):
    nama = models.CharField('nama', max_length=120, null=True)
    class Meta:
        db_table = "act"

    def __str__(self):
        return self.nama

class regist(models.Model):
    nama = models.CharField('nama', max_length=50, null=True, blank=False)
    act = models.ForeignKey(act, on_delete=models.CASCADE)

    class Meta:
        db_table = 'regist'

    def __str__(self):
        return self.nama



# Register your models here.
from django.contrib import admin
from .models import act, regist


admin.site.site_header = "Admin Dashboard"
# Register your models here.
@admin.register(act)

class actAdmin(admin.ModelAdmin):
    list_display = ('nama',)

@admin.register(regist)

class regAdmin(admin.ModelAdmin):
    list_display = ('nama', 'act',)


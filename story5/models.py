from django.forms import ModelForm, Textarea
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

SEMESTER_CHOICES = ( 
    ("Gasal 2020/2021", "Gasal 2020/2021"),
    ("Genap 2019/2020", "Genap 2019/2020"), 
    ("Gasal 2019/2020", "Gasal 2019/2020"),
) 

class user(models.Model):
    matkul = models.CharField('Mata Kuliah', max_length=120, null=True)
    sks = models.IntegerField('Jumlah SKS', default=1,
        validators=[
                MaxValueValidator(8),
                MinValueValidator(1)
            ]
        )

    dosen = models.CharField('Dosen', max_length=120, blank=True)
    desc = models.TextField('Deskripsi', max_length=100, null=True)
    semester = models.CharField('Semester', 
        max_length=120, null=True,
        choices = SEMESTER_CHOICES, 
        default = 'Gasal 2020/2021'
        )  

    ruang = models.CharField('Ruang Kelas', max_length=120, blank=True, default='Online Class')

    def __str__(self):
            return f'{self.matkul}'


    class Meta:
        db_table = "user"

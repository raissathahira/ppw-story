from django.contrib import admin
from .models import user


admin.site.site_header = "Admin Dashboard"
# Register your models here.
@admin.register(user)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id','matkul', 'dosen', 'sks', 'semester', 'ruang', 'desc')



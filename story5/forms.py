from django.core import validators
from django import forms

from .models import user

class jadwalform(forms.ModelForm):
    required_css_class = "required"
    class Meta:
        model = user
        fields = ['id','matkul', 'dosen','sks','semester','ruang','desc']
        widgets ={
            'matkul' : forms.TextInput(attrs={'class': 'form-control'}),
            'dosen' : forms.TextInput(attrs={'class': 'form-control'}),
            'sks' : forms.TextInput(attrs={'class': 'form-control'}),
            'semester' : forms.TextInput(attrs={'class': 'form-control'}),
            'ruang' : forms.TextInput(attrs={'class': 'form-control'}),
            'desc' : forms.TextInput(attrs={'class': 'form-control'}),
        }
        
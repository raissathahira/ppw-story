from django.shortcuts import render, redirect
from .forms import jadwalform
from .models import user


from django.http import HttpResponseRedirect
# Create your views here.
def home(request):
    return render(request, 'story5.html')
def story(request):
    return render(request, 'story.html')

def forms(request):
    return render(request, 'agenda.html')

def agenda(request):
    form = jadwalform(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/tambah')
    else:
        return render(request, 'agenda.html', {'form': form})

def data(request):
    if request.method =="POST":
        stud = user.objects.all()
        user.objects.get(id=request.POST['id']).delete() 
        return HttpResponseRedirect ('/jadwal') 
    else:
        stud = user.objects.all()
        return render(request,'data.html',{'stud' : stud})

def detail(request, index):
        st = user.objects.get(pk=index)
        return render(request, 'list.html', {'st': st})

    





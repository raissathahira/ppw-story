from django.urls import path
from . import views
from django.contrib import admin

app_name = 'story5'

urlpatterns = [
    path('', views.home, name='home'),
    path('story/', views.story, name='story'),
    path('tambah/', views.agenda, name= 'tambah'),
    path('tambah/data', views.agenda, name= 'tambah'),
    path('jadwal/', views.data, name= 'jadwal'),
    path('jadwal/submit', views.data, name= 'jadwal'),
    path('jadwal/<int:index>', views.detail, name="detail"),
]


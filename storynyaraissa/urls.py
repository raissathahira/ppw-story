from django.urls import path
from . import views

app_name = 'storynyaraissa'

urlpatterns = [
    path('story1', views.story1, name='story1'),
    path('portfolio', views.portfolio, name='portfolio'),
    path('contact', views.contact, name='contact'),
    # dilanjutkan ...
]
from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
import requests



# Create your views here.
def bookcatalogue(request):
    return render(request, 'catalogue.html')

def getdata(request):
    query = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + query
    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)

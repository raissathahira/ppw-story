from django.contrib import admin
from django.urls import path, include
from . import views


app_name = 'story8'

urlpatterns = [
    path('bookcatalogue', views.bookcatalogue, name= 'book'),
    path('getData', views.getdata),
]
 
from django.test import TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from . import views

# Create your tests here.
class Story8UnitTest(TestCase):
    def test_url_catalogue_data_is_exist(self):
        response = Client().get("/bookcatalogue")
        self.assertEquals(200, response.status_code)

    def test_template_form(self):
        response = Client().get("/bookcatalogue")
        self.assertTemplateUsed(response, "catalogue.html")

    def test_cataalogue_using_right_view_in_html(self):
        response = Client().get("/bookcatalogue")
        isi_html = response.content.decode("utf8")
        self.assertIn('<table class="table" id="dataTable" width="100%" cellspacing="0">', isi_html)

    def test_url_getdata_is_exist(self):
        response = Client().get("/getData?key=api")
        self.assertEquals(200, response.status_code)



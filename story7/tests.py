# Create your tests here.
from django.test import TestCase
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect
from .views import accord

class AccordUnitTest(TestCase):

    def test_accordion_page_url_is_exist(self):
        response = self.client.get('/accord/')
        self.assertEqual(response.status_code, 200)

    def test_accordion_views_render_the_correct_template(self):
        response = self.client.get('/accord/')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_accordion_using_the_right_views_function(self):
        found = resolve('/accord/')
        self.assertEqual(found.func, accord)


